package com.runicrealms.lockpickingdemo.Utils;

import com.runicrealms.lockpickingdemo.PlayerWrapper;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GuiUtil {


    public static ItemStack background() {
        ItemStack item = new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(StringUtils.colorCode("&r"));

        item.setItemMeta(meta);
        return item;

    }
    public static ItemStack border() {
        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(StringUtils.colorCode("&r"));

        item.setItemMeta(meta);
        return item;

    }

    public static ItemStack choose() {
        ItemStack item = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(StringUtils.colorCode("&r"));

        item.setItemMeta(meta);
        return item;

    }
    public static ItemStack correct() {
        ItemStack item = new ItemStack(Material.LIME_STAINED_GLASS_PANE, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(StringUtils.colorCode("&r"));

        item.setItemMeta(meta);
        return item;

    }
    public static ItemStack confirm() {
        ItemStack item = new ItemStack(Material.SLIME_BALL, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(StringUtils.colorCode("&r&aConfirm"));

        item.setItemMeta(meta);
        return item;
    }
    public static void fillInventory(Inventory inventory, ItemStack itemStack){
        for(int i = 0;i<53;i++){
            if(inventory.getItem(i)==null){
                inventory.setItem(i,itemStack);
            }
        }
    }

}
