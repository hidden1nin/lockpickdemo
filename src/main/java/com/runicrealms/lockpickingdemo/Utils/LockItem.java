package com.runicrealms.lockpickingdemo.Utils;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class LockItem {
    private final ItemStack item;
    private final Lock lock;

    private final NamespacedKey key = new NamespacedKey(LockPickingDemo.getPlugin(), "lockID");

    public LockItem(Lock lock){
        this.lock = lock;
        this.item = this.makeItem(lock);
    }

    public ItemStack makeItem(Lock lock) {
        ItemStack lockItem = new ItemStack(lock.getMaterial());
        ItemMeta itemMeta = lockItem.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GRAY+""+StringUtils.colorCode(lock.getName()));
        List<String> lore = new ArrayList<>();
        lore.add(StringUtils.colorCode("&r&f&m&o----------------------------------"));
        lore.add(StringUtils.colorCode("&r"));
        for (String string : this.lock.getDescription()) {
            lore.add(StringUtils.colorCode(string));
        }
        lore.add(StringUtils.colorCode("&7Lock Tier : "+this.lock.getTier().getLockName()));
        lore.add(StringUtils.colorCode("&7Lock Pins : "+this.lock.getPins()));
        lore.add(StringUtils.colorCode("&r&f&m&o----------------------------------"));
        itemMeta.setLore(lore);
        itemMeta.getPersistentDataContainer().set(this.key, PersistentDataType.STRING, lock.getReference().toUpperCase().replace(' ', '_'));
        lockItem.setItemMeta(itemMeta);
        return lockItem;
    }
    public Lock getLock(){
        return this.lock;
    }
}
