package com.runicrealms.lockpickingdemo.GUI.Rewards;


public enum RewardsEnum {
    BASIC(new BasicLockRewards()),
    MEDIUM(new MediumLockRewards()),
    HARD(new HardLockRewards());

    private final Rewards rewards;
    RewardsEnum(Rewards rewards){
        this.rewards = rewards;
    }
    public Rewards getRewards(){
        return this.rewards;
    }
}
