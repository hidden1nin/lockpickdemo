package com.runicrealms.lockpickingdemo.GUI.Rewards;

import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class BasicLockRewards extends Rewards{
    public BasicLockRewards(){
        super(Bukkit.createInventory(null,18), StringUtils.colorCode("&dCommon Reward"));
        this.getOriginalInventory().addItem(new ItemStack(Material.IRON_INGOT));
    }
}
