package com.runicrealms.lockpickingdemo.GUI.Rewards;

import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class HardLockRewards extends Rewards {
    public HardLockRewards(){
        super(Bukkit.createInventory(null,9), StringUtils.colorCode("&dHard Reward"));
        this.getOriginalInventory().addItem(new ItemStack(Material.DIAMOND));
    }
}
