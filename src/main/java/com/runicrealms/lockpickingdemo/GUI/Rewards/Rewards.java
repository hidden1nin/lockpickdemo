package com.runicrealms.lockpickingdemo.GUI.Rewards;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Rewards implements InventoryHolder {
    private final Inventory reward;
    private final String title;

    public Rewards(Inventory reward, String title) {
        this.reward = reward;
        this.title = title;
    }
    public Inventory getOriginalInventory() {
        return this.reward;
    }
    public Inventory getInventory() {
        Inventory clone = Bukkit.createInventory(this.reward.getHolder(), this.reward.getSize(), this.getTitle());
        for (ItemStack itemStack : this.reward.getContents()) {
            if (itemStack != null)
                clone.addItem(itemStack);
        }
        return clone;
    }

    public String getTitle() {
        return this.title;
    }
}
