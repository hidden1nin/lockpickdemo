package com.runicrealms.lockpickingdemo.GUI.Rewards;

import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class MediumLockRewards extends Rewards{
    public MediumLockRewards(){
        super(Bukkit.createInventory(null,36), StringUtils.colorCode("&dMedium Reward"));
        this.getOriginalInventory().addItem(new ItemStack(Material.COAL));
    }
}
