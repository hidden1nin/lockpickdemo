package com.runicrealms.lockpickingdemo.GUI;

import com.runicrealms.lockpickingdemo.Enums.LockEnum;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import com.runicrealms.lockpickingdemo.PlayerWrapper;
import com.runicrealms.lockpickingdemo.Utils.GuiUtil;
import com.runicrealms.lockpickingdemo.Utils.LockItem;
import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class LockPickingPracticeSelector implements InventoryHolder {

    private final Inventory inventory;
    private final PlayerWrapper player;

    private final ItemStack background = GuiUtil.background();

    private final NamespacedKey key = new NamespacedKey(LockPickingDemo.getPlugin(), "lockID");

    public LockPickingPracticeSelector(PlayerWrapper player){
        this.inventory = Bukkit.createInventory(this, 54, StringUtils.colorCode("&r" + player.getSkillLevel().getSkillTitle() + "'s&r Practice Locks"));
        this.player = player;

    }
    public void loadUI() {
        this.inventory.clear();

        int[] backgroundSlots = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48,
                49, 50, 51, 52, 53};
        for (int backgroundSlot : backgroundSlots) {
            this.inventory.setItem(backgroundSlot, this.background);
        }
        LockEnum[] locks = LockEnum.values();
        int[] lockSlots = new int[]{10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30,
                31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
        for (int i = 0; i < lockSlots.length && i < locks.length; i++) {
            this.inventory.setItem(lockSlots[i], new LockItem(locks[i].getLock()).makeItem(locks[i].getLock()));
        }

    }
        @Override
    public Inventory getInventory() {
        return this.inventory;
    }
    public PlayerWrapper getPlayer() {
        return this.player;
    }

    public NamespacedKey getId() {
        return key;
    }
}
