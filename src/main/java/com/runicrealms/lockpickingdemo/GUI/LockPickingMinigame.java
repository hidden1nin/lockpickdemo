package com.runicrealms.lockpickingdemo.GUI;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Games.LockPickGame;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import com.runicrealms.lockpickingdemo.PlayerWrapper;
import com.runicrealms.lockpickingdemo.Utils.GuiUtil;
import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class LockPickingMinigame implements InventoryHolder {
    private final Inventory inventory;
    private final PlayerWrapper player;
    private final Lock lock;
    private final int pins;

    private final ItemStack background = GuiUtil.background();

    private final ItemStack border = GuiUtil.border();
    public LockPickingMinigame(PlayerWrapper player, Lock lock) {
        this.player = player;
        this.inventory = Bukkit.createInventory(this, 54, StringUtils.colorCode("&e"+ player.getPlayer().getName() + "'s&r "+lock.getName()));
        this.pins = lock.getPins();
        this.lock = lock;
    }
    public PlayerWrapper getPlayer() {
        return this.player;
    }
    @Override
    public Inventory getInventory() {
        return this.inventory;
    }
    public Lock getLock(){
        return this.lock;
    }
    public int getPins(){
        return this.pins;
    }

    public void startGame() {
        this.loadUI();
        int speed = this.lock.getTier().getLockSpeed()*this.player.getSkillLevel().getSkillLevel();


        LockPickGame game = new LockPickGame(this);
        game.runTaskTimer(LockPickingDemo.getPlugin(), 0, speed);
        LockPickingDemo.getGameManager().getTasks().put(this.player.getPlayer(), game);
    }

    public void loadUI() {

        this.inventory.clear();

        this.inventory.setItem(4,GuiUtil.confirm());
        int[] borderSlots = new int[]{0, 1, 2, 3, 5, 6, 7, 8, 45, 46, 47, 48, 49, 50, 51, 52, 53};
        for (int borderSlot : borderSlots) {
            this.inventory.setItem(borderSlot, this.border);
        }
        GuiUtil.fillInventory(this.inventory,this.background);
        this.inventory.setItem(49,GuiUtil.correct());
    }

}
