package com.runicrealms.lockpickingdemo.Listeners;

import com.runicrealms.lockpickingdemo.GUI.LockPickingMinigame;
import com.runicrealms.lockpickingdemo.Games.LockPickGame;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.List;

public class CloseGameEvent implements Listener {
    @EventHandler
    public void onCloseGame(InventoryCloseEvent inventoryCloseEvent){
        if(inventoryCloseEvent.getInventory().getHolder()instanceof LockPickingMinigame){
            Player player = (Player) inventoryCloseEvent.getPlayer();
            if( LockPickingDemo.getGameManager().getTasks().containsKey(player)) {
                LockPickingDemo.getGameManager().getTasks().get(player).cancel();

            }
        }
    }
}
