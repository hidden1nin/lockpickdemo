package com.runicrealms.lockpickingdemo.Listeners;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Enums.LockEnum;
import com.runicrealms.lockpickingdemo.GUI.LockPickingMinigame;
import com.runicrealms.lockpickingdemo.GUI.LockPickingPracticeSelector;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.persistence.PersistentDataType;

public class SelectionClickListener implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!(event.getView().getTopInventory().getHolder() instanceof LockPickingPracticeSelector)) {
            return;
        }


        event.setCancelled(true);

        if (!(event.getClickedInventory().getHolder() instanceof LockPickingPracticeSelector)) {
            return;
        }

        LockPickingPracticeSelector lockPickingPracticeSelector = (LockPickingPracticeSelector) event.getClickedInventory().getHolder();
        if (!event.getCurrentItem().hasItemMeta() || !event.getCurrentItem().getItemMeta().getPersistentDataContainer().has(lockPickingPracticeSelector.getId(), PersistentDataType.STRING)) {
            return;
        }

        Lock lock = LockEnum.valueOf(event.getCurrentItem().getItemMeta().getPersistentDataContainer().get(lockPickingPracticeSelector.getId(), PersistentDataType.STRING)).getLock();
        Player player = (Player) event.getWhoClicked();
        player.closeInventory();
        LockPickingMinigame ui = new LockPickingMinigame(LockPickingDemo.getPlayers().get(player.getUniqueId()), lock);
        player.openInventory(ui.getInventory());
        ui.startGame();

    }
    }
