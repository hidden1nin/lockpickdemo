package com.runicrealms.lockpickingdemo.Listeners;

import com.runicrealms.lockpickingdemo.GUI.LockPickingMinigame;
import com.runicrealms.lockpickingdemo.Games.LockPickGame;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import com.runicrealms.lockpickingdemo.Utils.GuiUtil;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class LockPickingClick implements Listener {

    private final ItemStack correct = GuiUtil.correct();
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!(event.getView().getTopInventory().getHolder() instanceof LockPickingMinigame)) {
            return;
        }

        event.setCancelled(true);

        if (event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.SLIME_BALL) {
            return;
        }

        LockPickingMinigame lockPickingMinigame = (LockPickingMinigame) event.getView().getTopInventory().getHolder();
        LockPickGame game = LockPickingDemo.getGameManager().getTasks().get(lockPickingMinigame.getPlayer().getPlayer());
        game.setPins(game.getPins() - 1);
        game.getLockPickingMinigame().getInventory().setItem(game.getCurrentSlot(),correct);
        if (game.getDirection() > 0) {
            if (game.getTimer() != 5) {
                game.setPins(game.getLockPickingMinigame().getPins());
                game.getLockPickingMinigame().loadUI();
            }
        } else {
            if (game.getTimer() != 3) {
                game.setPins(game.getLockPickingMinigame().getPins());
                game.getLockPickingMinigame().loadUI();
            }
        }

    }
}
