package com.runicrealms.lockpickingdemo.Enums;

import com.runicrealms.lockpickingdemo.GUI.Rewards.Rewards;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.List;

public class Lock implements InventoryHolder {
    private final String name;
    private final Material material;
    private final LockTier lockTier;
    private final Integer pins;
    private final List<String> description;
    private final LockSuccess lockSuccess;
    private final LockFail lockFail;
    private final Rewards rewards;
    //same as enum class name, allows us to store enum in items and retrive it later
    private final String reference;

    public Lock(String reference,String name, Material material, LockTier lockTier,Integer pins, List<String> description, LockSuccess lockSuccess, LockFail lockFail, Rewards rewards){
        this.reference = reference;
        this.name = name;
        this.material = material;
        this.lockTier = lockTier;
        this.pins = pins;
        this.description = description;
        this.lockSuccess = lockSuccess;
        this.lockFail = lockFail;

        this.rewards = rewards;
    }

    public String getReference() {
        return this.reference;
    }
    public String getName() {
        return this.name;
    }

    public Material getMaterial() {
        return this.material;
    }

    public LockTier getTier() {
        return this.lockTier;
    }
    public Integer getPins(){
        return this.pins;
    }
    public LockSuccess getLockSuccess(){
        return this.lockSuccess;
    }
    public LockFail getLockFail(){
        return this.lockFail;
    }
    public List<String> getDescription() {
        return this.description;
    }
    public Rewards getRewards() {
        return rewards;
    }

    @Override
    public Inventory getInventory()
    {
        return this.rewards.getInventory();
    }
}
