package com.runicrealms.lockpickingdemo.Enums;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Enums.Locks.Basic;
import com.runicrealms.lockpickingdemo.Enums.Locks.Hard;
import com.runicrealms.lockpickingdemo.Enums.Locks.Medium;

public enum LockEnum {
    BASIC(new Basic()),
    MEDIUM(new Medium()),
    HARD(new Hard());
    private final Lock lock;
    LockEnum(Lock lock){
        this.lock = lock;
    }
    public Lock getLock(){
        return this.lock;
    }

}
