package com.runicrealms.lockpickingdemo.Enums;

import org.bukkit.entity.Player;

@FunctionalInterface
public interface LockSuccess {
    void effectLockSuccess(Player player);
}
