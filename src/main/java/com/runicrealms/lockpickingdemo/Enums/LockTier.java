package com.runicrealms.lockpickingdemo.Enums;

public enum  LockTier {
    COMMON(20,"&aCommon"),
    AVERAGE(10, "&bAverage"),
    EPIC(5, "&cEpic");
    private final int speed;
    private final String lockName;
    LockTier(int speed, String lockName){
        this.speed = speed;
        this.lockName = lockName;
    }
    public int getLockSpeed(){
        return this.speed;
    }
    public String getLockName(){
        return this.lockName;
    }
}
