package com.runicrealms.lockpickingdemo.Enums;

import org.bukkit.entity.Player;

@FunctionalInterface
public interface LockFail {
    void effectLockFail(Player player);
}
