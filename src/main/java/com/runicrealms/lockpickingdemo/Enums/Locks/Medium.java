package com.runicrealms.lockpickingdemo.Enums.Locks;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Enums.LockTier;
import com.runicrealms.lockpickingdemo.GUI.Rewards.MediumLockRewards;
import com.runicrealms.lockpickingdemo.GUI.Rewards.RewardsEnum;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collections;

public class Medium extends Lock {
    public Medium(){
        super("Medium","Medium Lock", Material.CHEST, LockTier.AVERAGE,2, Collections.singletonList("&7A little harder"),(Player player)->{
            player.sendMessage("Lock picked!");
        },(Player player)->{player.sendMessage("Lock pick failed!");}, RewardsEnum.MEDIUM.getRewards());
    }
}
