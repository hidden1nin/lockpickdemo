package com.runicrealms.lockpickingdemo.Enums.Locks;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Enums.LockTier;
import com.runicrealms.lockpickingdemo.GUI.Rewards.BasicLockRewards;
import com.runicrealms.lockpickingdemo.GUI.Rewards.HardLockRewards;
import com.runicrealms.lockpickingdemo.GUI.Rewards.RewardsEnum;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collections;

public class Hard extends Lock {
    public Hard(){
        super("Hard","Hard Lock", Material.IRON_BARS, LockTier.EPIC,3, Collections.singletonList("&7Very Difficult"),(Player player)->{
            player.sendMessage("Hard Lock picked!");
        },(Player player)->{player.sendMessage("Hard Lock pick failed!");}, RewardsEnum.HARD.getRewards());

    }

}
