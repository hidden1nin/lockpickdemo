package com.runicrealms.lockpickingdemo.Enums.Locks;

import com.runicrealms.lockpickingdemo.Enums.Lock;
import com.runicrealms.lockpickingdemo.Enums.LockTier;
import com.runicrealms.lockpickingdemo.GUI.Rewards.BasicLockRewards;
import com.runicrealms.lockpickingdemo.GUI.Rewards.RewardsEnum;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collections;

public class Basic extends Lock{
    public Basic(){
        super("Basic","Basic Lock", Material.CHEST, LockTier.COMMON,1, Collections.singletonList("&7You have to start somewhere"),(Player player)->{
            player.sendMessage("Lock picked!");
        },(Player player)->{player.sendMessage("Lock pick failed!");}, RewardsEnum.BASIC.getRewards());

    }

}
