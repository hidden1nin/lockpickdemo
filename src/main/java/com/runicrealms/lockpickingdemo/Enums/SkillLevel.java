package com.runicrealms.lockpickingdemo.Enums;

public enum SkillLevel {
    BEGINNER(1,"&aBeginner"),
    APPRENTICE(2, "&bApprentice"),
    MASTER(3, "&cMaster");
    private final int skillLevel;
    private final String skillTitle;
    SkillLevel(int skillLevel, String skillTitle){
        this.skillLevel = skillLevel;
        this.skillTitle = skillTitle;
    }
    public int getSkillLevel(){
        return this.skillLevel;
    }
    public String getSkillTitle(){
        return this.skillTitle;
    }

}
