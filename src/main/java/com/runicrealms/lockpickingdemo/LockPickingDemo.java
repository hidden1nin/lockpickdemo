package com.runicrealms.lockpickingdemo;

import com.runicrealms.lockpickingdemo.Commands.CreatePlayerWrapperCommand;
import com.runicrealms.lockpickingdemo.Commands.PracticeCommand;
import com.runicrealms.lockpickingdemo.Games.GameManager;
import com.runicrealms.lockpickingdemo.Listeners.CloseGameEvent;
import com.runicrealms.lockpickingdemo.Listeners.LockPickingClick;
import com.runicrealms.lockpickingdemo.Listeners.SelectionClickListener;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

public final class LockPickingDemo extends JavaPlugin {
    private static Plugin plugin;
    private static GameManager gameManager;
    private static HashMap<UUID, PlayerWrapper> players;

    public static GameManager getGameManager() {
        return gameManager;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic

        plugin = this;

        players = new HashMap<>();

        Bukkit.getPluginCommand("createplayer").setExecutor(new CreatePlayerWrapperCommand());

        Bukkit.getPluginCommand("practice").setExecutor(new PracticeCommand());
        registerEvents(new SelectionClickListener(),new CloseGameEvent(), new LockPickingClick());
        gameManager = new GameManager();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
    public static Plugin getPlugin(){
        return plugin;
    }
    public static HashMap<UUID, PlayerWrapper> getPlayers() {
        return players;
    }

    private void registerEvents(Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, this);
        }
    }
}
