package com.runicrealms.lockpickingdemo;

import com.runicrealms.lockpickingdemo.Enums.SkillLevel;
import org.bukkit.entity.Player;

public class PlayerWrapper {

    private final Player player;
    private final SkillLevel skillLevel;


    public PlayerWrapper(Player player,SkillLevel skillLevel) {
        this.player = player;
        this.skillLevel = skillLevel;
    }
    public Player getPlayer(){
        return this.player;
    }

    public SkillLevel getSkillLevel(){
        return this.skillLevel;
    }

}
