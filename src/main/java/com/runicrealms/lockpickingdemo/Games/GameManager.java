package com.runicrealms.lockpickingdemo.Games;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class GameManager {
    private final HashMap<Player, LockPickGame> tasks;

    public GameManager() {
        this.tasks = new HashMap<>();
    }

    public HashMap<Player, LockPickGame> getTasks() {
        return this.tasks;
    }

}
