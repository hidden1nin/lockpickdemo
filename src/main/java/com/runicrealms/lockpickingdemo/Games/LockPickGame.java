package com.runicrealms.lockpickingdemo.Games;

import com.runicrealms.lockpickingdemo.GUI.LockPickingMinigame;
import com.runicrealms.lockpickingdemo.Utils.GuiUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class LockPickGame extends BukkitRunnable {
    private final LockPickingMinigame ui;
    private final Inventory inventory;

    private final ItemStack background = GuiUtil.background();
    private final ItemStack choose = GuiUtil.choose();
    private final ItemStack correct = GuiUtil.correct();
    private int timer;
    private int direction;
    private int pinsLeft;
    private int currentSlot = 0;
    public LockPickGame(LockPickingMinigame ui) {
        this.ui = ui;
        this.inventory = ui.getInventory();
        this.timer = 0;
        this.direction = 1;
        this.pinsLeft = ui.getPins();
    }

    public void setPins(int newPins) {
        this.pinsLeft = newPins;
    }

    public int getPins() {
        return this.pinsLeft;
    }
    public int getTimer(){return this.timer;}
    public int getDirection(){return this.direction;}
    public int getCurrentSlot(){return this.currentSlot;}
    public LockPickingMinigame getLockPickingMinigame(){
        return this.ui;
    }
    @Override
    public void run() {
        if (this.getPins() <= 0) {
            this.cancel();
            this.getLockPickingMinigame().getLock().getLockSuccess().effectLockSuccess(ui.getPlayer().getPlayer());
            ui.getPlayer().getPlayer().openInventory(this.getLockPickingMinigame().getLock().getRewards().getInventory());
            return;
        }
        int startingSlot = 9 * this.getPins();
        int slot = this.timer + startingSlot;

        if (slot - (direction) < 9 + startingSlot && slot - direction > startingSlot - 1)
            this.inventory.setItem(slot - (direction), this.background);

        if (slot > 8 + startingSlot) {
            this.direction = -1;
            slot--;
            slot--;
            this.timer += direction;
        }
        if (slot < startingSlot) {
            this.direction = 1;
            this.timer = 0;
            slot++;
            slot++;
            this.timer += direction;
        }

        this.timer += direction;
        this.inventory.setItem(slot, this.choose);
        this.currentSlot = slot;
        Player player = ui.getPlayer().getPlayer();

        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1, 1); //test


    }
}
