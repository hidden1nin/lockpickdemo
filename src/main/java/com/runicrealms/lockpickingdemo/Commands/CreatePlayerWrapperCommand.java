package com.runicrealms.lockpickingdemo.Commands;

import com.runicrealms.lockpickingdemo.Enums.SkillLevel;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import com.runicrealms.lockpickingdemo.PlayerWrapper;
import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreatePlayerWrapperCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(StringUtils.colorCode("&r&cOnly players can execute this command!"));
            return false;
        }

        Player player = (Player) sender;

        if (LockPickingDemo.getPlayers().containsKey(player.getUniqueId())) {
            player.sendMessage(StringUtils.colorCode("&7Your are already a "+LockPickingDemo.getPlayers().get(player.getUniqueId()).getSkillLevel().getSkillTitle()));
            return true;
        }

        LockPickingDemo.getPlayers().put(player.getUniqueId(), new PlayerWrapper(player, SkillLevel.BEGINNER));
        player.sendMessage(StringUtils.colorCode("&7Your a "+SkillLevel.BEGINNER.getSkillTitle()));

        return true;
    }

}
