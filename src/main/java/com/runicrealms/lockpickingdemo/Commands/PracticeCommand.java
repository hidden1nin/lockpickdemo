package com.runicrealms.lockpickingdemo.Commands;

import com.runicrealms.lockpickingdemo.Enums.SkillLevel;
import com.runicrealms.lockpickingdemo.GUI.LockPickingPracticeSelector;
import com.runicrealms.lockpickingdemo.LockPickingDemo;
import com.runicrealms.lockpickingdemo.PlayerWrapper;
import com.runicrealms.lockpickingdemo.Utils.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PracticeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(StringUtils.colorCode("&r&cOnly players can execute this command!"));
            return false;
        }
        Player player = (Player) sender;

        if (!LockPickingDemo.getPlayers().containsKey(player.getUniqueId())) {
            LockPickingDemo.getPlayers().put(player.getUniqueId(), new PlayerWrapper(player, SkillLevel.BEGINNER));
            player.sendMessage(StringUtils.colorCode("&7Defaulted to a "+SkillLevel.BEGINNER.getSkillTitle()));
        }

        PlayerWrapper playerWrapper = LockPickingDemo.getPlayers().get(player.getUniqueId());

        LockPickingPracticeSelector practiceSelector = new LockPickingPracticeSelector(playerWrapper);
        practiceSelector.loadUI();
        playerWrapper.getPlayer().openInventory(practiceSelector.getInventory());
        return false;
    }
}
